import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToDoItem } from '../models/toDoItem';

@Injectable({
  providedIn: 'root'
})
export class TodoService {  

  constructor(private http: HttpClient) { }

  getTodoItems():Observable<ToDoItem[]> {
   
    return this.http.get<ToDoItem[]>('https://jsonplaceholder.typicode.com/todos?userId=1');
  }
}