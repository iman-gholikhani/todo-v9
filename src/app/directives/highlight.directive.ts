import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private el: ElementRef) {  }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(  'linear-gradient(to right, rgba(255,255,255,0.2) , rgba(255,255,255,0))'    );
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }

  private highlight(bg: string) {
    this.el.nativeElement.style.background = bg;  }
}
