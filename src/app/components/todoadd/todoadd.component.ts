import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todoadd',
  templateUrl: './todoadd.component.html',
  styleUrls: ['./todoadd.component.scss']
})
export class TodoaddComponent implements OnInit {

  todo:string;
  buttonStatus: boolean = false;
  @Output() notify = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  onClick() {
    if(this.buttonStatus) {      
      this.notify.emit(this.todo);
      this.todo = "";
    } 
    this.buttonStatus = !this.buttonStatus;
  }
}