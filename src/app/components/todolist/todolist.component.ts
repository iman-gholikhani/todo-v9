import { Component, OnInit } from '@angular/core';
import { ToDoItem } from '../../models/toDoItem';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {
  
  toDoList: ToDoItem[];
  isMouseOver: number = -1;

  constructor(private todoService: TodoService) { }

  ngOnInit() { 
    this.getTodoData();
  }
  getTodoData():void {
     this.todoService.getTodoItems().subscribe(toDoList => this.toDoList = toDoList);
  }
  onOver(index) {
    this.isMouseOver = index; 
  }
  onMouseLeave() {
    this.isMouseOver = -1;
  }
  onToggle(item) {
     this.toDoList.filter(obj => obj == item)[0].isDone = !this.toDoList.filter(obj => obj == item)[0].isDone;
  }
  onRemove(item) {
    this.toDoList.splice(this.toDoList.indexOf(item),1);
  }
  onAdd(newTodo) { 
    this.toDoList.unshift( { title: newTodo , isDone: false } );  
  } 
    
}